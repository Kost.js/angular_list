import { Component, Output, EventEmitter } from '@angular/core';
import { DataService } from './services/data.service';

@Component({
  selector: 'my-app',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
  providers: [DataService]
})
export class AppComponent {
    title: string = 'List';
    items: { text: string, change: boolean}[] = [];
    itemChange: boolean = true;
      constructor() {}
      addNewItem(item: { text: string, change: boolean}) {
        console.log('New item is added');
        this.items.push(item);
      }
      registerChange(isAllow: boolean) {
          this.itemChange = isAllow;
      }
      changeStatus(data: {id: number, state: boolean}) {
          this.items = this.items.map ((el, ind) => {
                if (ind !== data.id) el.change = data.state;
                return el;
          });
      }
}

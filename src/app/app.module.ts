import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core';
import { AppComponent } from './app.component';
import { PanelComponent } from './components/panel-component/panel.component';
import { ListComponent } from './components/list-component/list.component';
import { ItemComponent } from './components/list-component/item-component/item.component';


@NgModule({
  bootstrap: [AppComponent],
  declarations: [
  AppComponent,
  PanelComponent,
  ListComponent,
  ItemComponent
  ],
  imports: [
    CoreModule,
    BrowserModule
  ],
})
export class AppModule {}

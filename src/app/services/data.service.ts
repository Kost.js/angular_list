export class DataService {
  private data: string[] = [];// ['First', 'Second'];
  ind: number = -1;
  constructor() {}
  addData(str: string = 'New Item') {
    console.log(str);
    this.data.push(str);
  }
  
  getData(): string[] {
    return this.data;
  }

  changeValue(txt: string) {
    this.data[this.ind] = txt;
  }
  
  moveUp() {
      if(this.ind > 0) {
      let text = this.data[this.ind];
      this.data[this.ind] = this.data[this.ind-1];
      this.data[--this.ind] = text;
      console.log(this.ind);
      }
  }
  
  moveDown() {
    if(this.ind < this.data.length-1) {
      let text = this.data[this.ind];
      this.data[this.ind] = this.data[this.ind+1];
      this.data[++this.ind] = text;
      console.log(this.ind);
    }
  }

  removeData() {
    this.data.splice(this.ind, 1);
    this.ind = -1;
  }
}
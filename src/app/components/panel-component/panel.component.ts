import { Component, Input, Output, EventEmitter, OnInit, AfterContentChecked} from '@angular/core';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html'
})
export class PanelComponent implements OnInit, AfterContentChecked {
  allow: boolean;
  isEditPresent: boolean;
  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.allow = true;
    this.isEditPresent = false;
  }

  ngAfterContentChecked() {
    this.allow = this.dataService.ind >= 0 ? false : true;
  }

  onCreateItem() {
    console.log('Create item');
    this.dataService.addData();
  }

  onDeleteItem() {
    console.log('Data delete');
    this.dataService.removeData();
  }

  onEditItem() {
    console.log('Edit item');
    this.isEditPresent = !this.isEditPresent;
    console.log(this.isEditPresent);
  }

  onMoveItemUp() {
    console.log('Move up');
    this.dataService.moveUp();
  }

  onMoveItemDown() {
    console.log('Move down');
    this.dataService.moveDown();
  }

  getCurrentValue(): string {
    return this.dataService
    .getData()[this.dataService.ind];
  }

  saveText(text: string) {
    this.dataService.changeValue(text);
    this.isEditPresent = !this.isEditPresent;
  }
}
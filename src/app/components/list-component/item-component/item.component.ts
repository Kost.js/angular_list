import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: 'item.component.html',
})

export class ItemComponent implements OnInit {
  @Input() itemData: {id: number, text: string, isDisabled: boolean, isChecked: boolean};
  @Output() index = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {
    console.log(this.itemData.id);
  }

  changeStatus(isItemChecked: boolean) {
      let ind = isItemChecked ? this.itemData.id : -1;
      this.index.emit(ind);
  }
}

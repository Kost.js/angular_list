import { Component, Input, Output, EventEmitter, OnInit, AfterContentChecked } from '@angular/core';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-list',
  templateUrl: 'list.component.html'
})

export class ListComponent implements OnInit, AfterContentChecked  {
  list: string[];
  disabledItems: boolean[];
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.list = this.dataService.getData();
    console.log('on init');
  }

  ngAfterContentChecked() {
    console.log(this.dataService.ind);
    this.disabledItems = 
    this.dataService.ind >= 0 ? this.list.map((el, ind) =>
      (ind === this.dataService.ind) ? false : true)
    : 
     Array(this.list.length).fill(false);
  }

  getIdPickedItem(index: number) {
    console.log('Picked ', index);
    this.dataService.ind = index;  
  }
  
  isItemChecked(indexItem: number) {
    return indexItem === this.dataService.ind ? true : false;
  }

  geItemObject(_id: number, _text: string) {
    return {
        id: _id, 
        text: _text,
        isDisabled: this.disabledItems[_id],
        isChecked: _id === this.dataService.ind ? true : false
        }
  }
}